import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { AgmCoreModule } from '@agm/core';
import { MapsRoutingModule } from "./maps-routing.module";
import { NgxSpinnerModule } from 'ngx-spinner';

import { FullScreenMapComponent } from "./full-screen-map/full-screen-map.component";
import { GoogleMapComponent } from "./google-map/google-map.component";

@NgModule({
    imports: [
        CommonModule,
        MapsRoutingModule,
        NgxSpinnerModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAYGj2fBIjTt5VdWF7U8KZxr2Rm9zXqUhE'
        })
    ],
    declarations: [
        FullScreenMapComponent,
        GoogleMapComponent
    ]
})
export class MapsModule { }
