import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-google-map',
    templateUrl: './google-map.component.html',
    styleUrls: ['./google-map.component.scss'],
})

export class GoogleMapComponent implements OnInit {

  center = {lat: 24.309024, lng:  -100.414523};
  map: google.maps.Map;
  tipoMapa: boolean;
  centerMun: any;
  anchoLinea = 0.5;
  colorLinea: any;

  constructor(private spinner: NgxSpinnerService) {}

  ngOnInit(): void {

    this.cargarMapa();

  }

  cargarMapa() {

    this.cargarSpinner();

    this.tipoMapa = false;
    // The map
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: this.center,
      zoom: 5
    });

    this.removerData();

    this.cargarJson('assets/data/mapaEstados.json');

    this.pintarMapa();

    this.mostrarDatosPantalla();

    // Carga los datos de los Municipios
    this.map.data.addListener('click', (event) => {
      if (event.feature.getProperty('municipio_cvegeo') === undefined) {
        this.cargarSpinner();
        this.removerData();
        this.tipoMapa = true;
        this.cargarJson( `assets/data/mapas-municipios/menos${event.feature.getProperty('entidad_nombre').replace(/\s/g, '')}.json` );
        this.cargarJson( `assets/data/mapas-municipios/mapa${event.feature.getProperty('entidad_nombre').replace(/\s/g, '')}.json` );
        event.feature.setProperty('state', 'hover');
        this.centerMun  = {lat: event.feature.getProperty('lat'), lng:  event.feature.getProperty('lng')};
        this.map.setCenter(this.centerMun);
        this.map.setZoom(event.feature.getProperty('zoom'));
      }
    });

  }

  cargarSpinner() {
    this.spinner.show(undefined,
      {
        type: 'ball-triangle-path',
        size: 'medium',
        bdColor: 'rgba(51, 51, 51, 0.8)',
        color: '#fff',
        fullScreen: false
    });
  }

  removerData() {
    this.map.data.forEach( (feature) => {
      this.map.data.remove(feature);
    });
  }

  cargarJson(json: string) {
    this.map.data.loadGeoJson(json);
  }

  pintarMapa() {
    // set up the style rules and events for google.maps.Data
    this.map.data.setStyle((feature) => {
      const color = [];

      switch (feature.getProperty('color')) {
        case 'red': { color[0] = '5'; color[1] = '69'; color[2] = '54';
          break;
        }
        case 'orange': { color[0] = '29'; color[1] = '59'; color[2] = '51';
          break;
        }
        case 'yellow': { color[0] = '54'; color[1] = '94'; color[2] = '48';
          break;
        }
        case 'green': { color[0] = '102'; color[1] = '78'; color[2] = '41';
          break;
        }
        default: { color[0] = '5'; color[1] = '69'; color[2] = '54';
          break;
        }
     }

     if (feature.getProperty('municipio_cvegeo') === undefined) {
      this.colorLinea = '#fff';
      this.anchoLinea = 0.5;
     } else {
      this.colorLinea = 'black';
      this.anchoLinea = 2.5;
     }

     this.spinner.hide();
      return {
        strokeWeight: this.anchoLinea,
        strokeColor: this.colorLinea,
        zIndex: 2,
        fillColor: 'hsl(' + color[0] + ',' + color[1] + '%,' + color[2] + '%)',
        fillOpacity: 0.75,
        visible: true
      };
    });
  }

  mostrarDatosPantalla() {
    this.map.data.addListener('mouseover', (event) => {
      event.feature.setProperty('state', 'hover');
      let porcentajeRiesgo = 100;

      if (event.feature.getProperty('color') === 'red') {
        porcentajeRiesgo = 10;
      } else if (event.feature.getProperty('color') === 'orange') {
        porcentajeRiesgo = 40;
      } else if (event.feature.getProperty('color') === 'yellow') {
        porcentajeRiesgo = 70;
      } else if (event.feature.getProperty('color') === 'green') {
        porcentajeRiesgo = 95;
      }

      document.getElementById('data-label').textContent = event.feature.getProperty('entidad_nombre');
      document.getElementById('data-value').textContent = event.feature.getProperty('entidad_cvegeo');

      document.getElementById('data-box').style.display = 'block';
      document.getElementById('data-caret').style.display = 'block';
      document.getElementById('data-caret').style.paddingLeft = porcentajeRiesgo + '%';
    });

    this.map.data.addListener('mouseout', () => {
      document.getElementById('data-box').style.display = 'none';
      document.getElementById('data-caret').style.display = 'none';
    });
  }

}
